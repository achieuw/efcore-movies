﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EFCore_movies.Models;
using EFCore_movies.Models.DTOs.Character;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCore_movies.Controllers
{
    [Route("api/characters")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(MovieCharacterDbContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        /// <summary>
        /// Gets all characters stored in the database.
        /// </summary>
        /// <returns>Returns all characters stored in the database.</returns>
        /// <response code="200">Returns all characters stored in the database.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]

        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = _mapper.Map<List<CharacterReadDTO>>(await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync());

            return Ok(characters);
        }

        /// <summary>
        /// Gets a specific character with the specified id
        /// </summary>
        /// <param name="id">Id of specified character</param>
        /// <returns>Returns the character with the specified id if it exists</returns>
        /// <response code="200">Returns the character with the specified id </response>
        /// <response code="404">If the item is null </response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            Character character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<CharacterReadDTO>(character));
        }

        /// <summary>
        /// Creates a new characters with the properties specified in the input object
        /// </summary>
        /// <param name="dtoCharacter">DTO form of the character to create</param>
        /// <returns>Returns a read DTO of the created character</returns>
        /// <response code="200">Returns a read DTO of the created character</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            _context.Add(domainCharacter);
            await _context.SaveChangesAsync();

            var readCharacter = CreatedAtAction("GetCharacter", new { id = domainCharacter.CharacterId },
                _mapper.Map<CharacterReadDTO>(domainCharacter));
            return Ok(readCharacter);
        }

        /// <summary>
        /// Replaces a specified character with a new version of it
        /// </summary>
        /// <param name="id">Id of specified character</param>
        /// <param name="dtoCharacter">DTO form of the character to replace the old character. Id in DTO must match id parameter</param>
        /// <returns></returns>
        /// <response code="204"> Character was successfully edited</response>
        /// <response code="400"> Given id and id in character DTO mismatch</response>
        /// <response code="404"> Character with input id does not exist</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.CharacterId)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);

            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }

            return NoContent();

        }

        /// <summary>
        /// Deletes the character with the input id if it exists
        /// </summary>
        /// <param name="id">Id of character to delete</param>
        /// <returns></returns>
        /// <response code="204">Character succesfully deleted</response>
        /// <response code="404">Character to delete not found</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }

        /// <summary>
        /// Adds a relationship in the database between the movie with the input id and the character with the input id
        /// </summary>
        /// <param name="id">Character id</param>
        /// <param name="movieId">Movie id</param>
        /// <returns></returns>
        /// <response code="204">Movie successfully added to character</response>
        /// <response code="404">Specified character or movie does not exist</response>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignMovieToCharacter(int id, [FromBody] int movieId)
        {
            var character = await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.CharacterId == id)
                .FirstAsync();

            var movie = await _context.Movies.FindAsync(movieId);

            if (character == null || movie == null)
            {
                return NotFound();
            }

            character.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}
