﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EFCore_movies.Models;
using EFCore_movies.Models.DTOs.Character;
using EFCore_movies.Models.DTOs.Franchise;
using EFCore_movies.Models.DTOs.Movie;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCore_movies.Controllers
{
    /// <summary>
    /// Franchise controller class with both generic CRUD operations and more explicit operations
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor passing the DbContext and mapper by dependency injection
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapper"></param>
        public FranchiseController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region CRUD operations
        /// <summary>
        /// Fetch all franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises
                .Include(f => f.Movies)
                .ToListAsync());

            return Ok(franchises);
        }

        /// <summary>
        /// Get a specific franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _context.Franchises.FindAsync(id);

            if(franchise == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<FranchiseReadDTO>(franchise));
        }

        /// <summary>
        /// Adds a new franchise to the DB
        /// </summary>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();

            var readFranchise = CreatedAtAction("GetFranchise", new { id = domainFranchise.FranchiseId },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
            return Ok(readFranchise);
        }

        /// <summary>
        /// Updates a franchise. Must pass a franchise id and object
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.FranchiseId)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        #endregion

        #region Relationship operations
        /// <summary>
        /// Assign movies to a franchise. Must pass franchise Id and an array of movie Ids
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task<ActionResult> AssignMovieToFranchise(int id, [FromBody] int[] movieIds)
        {
            var franchise = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.FranchiseId == id)
                .FirstAsync();

            foreach (var movieId in movieIds)
            {
                var movie = await _context.Movies.FindAsync(movieId);

                if (franchise == null || movie == null)
                {
                    return NotFound();
                }
                franchise.Movies.Add(movie);
            }

            await _context.SaveChangesAsync();

            return Ok();
        }
        
        /// <summary>
        /// Fetch all movies in a franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("franchiseMovie/{id}")]
        public async Task<ActionResult<List<MovieReadDTO>>> GetAllMovies(int id)
        {
            Franchise franchise = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.FranchiseId == id)
                .FirstAsync();

            if(franchise == null)
            {
                return NotFound();
            }

            var movies = _mapper.Map<List<MovieReadDTO>>(franchise.Movies);
            return Ok(movies);
        }

        /// <summary>
        /// Fetch all characters corresponding to the movies of the specific franchise by passing a franchise Id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("franchiseCharacter")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAllCharacters(int id)
        {
            HashSet<Character> characters = new HashSet<Character>();

            var movies = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.FranchiseId == id)
                .ToListAsync();
            

            foreach(var movie in movies)
            {
               await _context.Characters
                    .Where(c => c.Movies.Contains(movie))
                    .ForEachAsync(c => characters.Add(c));                                        
            }

            return Ok(_mapper.Map<List<CharacterReadDTO>>(characters.ToList()));
        }
        #endregion
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
    }
}
