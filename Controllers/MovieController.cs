﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EFCore_movies.Models;
using EFCore_movies.Models.DTOs.Character;
using EFCore_movies.Models.DTOs.Movie;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFCore_movies.Controllers
{
    [Route("api/movies")]
    [ApiController]
    public class MovieController: ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieCharacterDbContext context, IMapper mapper)
        {
            this._mapper = mapper;
            this._context = context;
        }

        /// <summary>
        /// Get a list of all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {            
            var movies = _mapper.Map<List<MovieReadDTO>>(await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync());

            return Ok(movies);
        }
        /// <summary>
        /// Get a specific movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _context.Movies.FindAsync(id);

            if(movie == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MovieReadDTO>(movie));
        }
        /// <summary>
        /// Create a movie
        /// </summary>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            _context.Add(domainMovie);
            await _context.SaveChangesAsync();

            var readMovie = CreatedAtAction("GetMovie", new { id = domainMovie.MovieId }, _mapper.Map<MovieReadDTO>(domainMovie));
            return Ok(readMovie);
        }
        /// <summary>
        /// Update a movie 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>

        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.MovieId)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);

            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }

            return NoContent();

        }
        /// <summary>
        /// Delete a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id )
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Assign characters to a specific movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="charactersIds"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task<ActionResult> AssignCharactersToMovie(int id, [FromBody] int[] charactersIds)
        {
            var movie = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();
            foreach(var characterId in charactersIds)
            {
                var character = await _context.Characters.FindAsync(characterId);

                if(character == null || movie == null)
                {
                    return NotFound();
                }
                movie.Characters.Add(character);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Get all characters from a specific movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("movieCharacter/{id}")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetAllCharacters(int id)
        {
            Movie movie = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();

            if(movie == null)
            {
                return NotFound();
            }

            var characters = _mapper.Map<List<CharacterReadDTO>>(movie.Characters);
            return Ok(characters);
        }
        /// <summary>
        /// Method to check if a movie exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
