﻿using System.Collections.Generic;

namespace EFCore_movies.Models.DTOs.Character
{
    public class CharacterReadDTO
    {
        public int CharacterId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public List<int> Movies { get; set; }
    }
}
