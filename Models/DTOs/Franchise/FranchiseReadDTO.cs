﻿using System.Collections.Generic;

namespace EFCore_movies.Models.DTOs.Franchise
{
    public class FranchiseReadDTO
    {
        public int FranchiseId { get; set;}
        public string Name { get; set;}
        public string Description { get; set;}
        public List<int> Movies { get; set;}
    }
}
