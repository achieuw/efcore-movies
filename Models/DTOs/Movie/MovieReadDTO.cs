﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EFCore_movies.Models;


namespace EFCore_movies.Models.DTOs.Movie
{
    public class MovieReadDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int Franchise { get; set; }
        public List<int> Characters { get; set; }
    }
}
