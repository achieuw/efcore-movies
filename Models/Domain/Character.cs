﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCore_movies.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(300)]
        public string Picture { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
