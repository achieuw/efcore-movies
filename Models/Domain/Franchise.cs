﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCore_movies.Models
{
    public class Franchise
    {
        public int FranchiseId { get; set;}
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
