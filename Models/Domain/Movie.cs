﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCore_movies.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }
        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [Required]
        [MaxLength(100)]
        public string Director { get; set; }
        [MaxLength(300)]
        public string Picture { get; set; }
        [MaxLength(300)]
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; } = new HashSet<Character>();
    }
}
