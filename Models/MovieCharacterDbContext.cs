﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace EFCore_movies.Models
{
    public class MovieCharacterDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharacterDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 1, Name = "Harry Potter", Description = "Harry James Potter (b. 31 July 1980) was an English half-blood wizard, and one of the most famous wizards of modern times. The only child and son of James and Lily Potter (née Evans), Harry's birth was overshadowed by a prophecy, naming either himself or Neville Longbottom as the one with the power to vanquish Lord Voldemort, the most powerful and feared Dark Wizard in the world. After half of the prophecy was reported to Voldemort, courtesy of Severus Snape, Harry was chosen as the target due to his many similarities with the Dark Lord. In turn, this caused the Potter family to go into hiding. " });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 2, Name = "Austin Powers", Description = "Guy with alot of mojo" });

            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 1, Title = "The Room", Genre = "Unintentional Comedy/Romance", ReleaseYear = 2003, Director = "Tommy Wiseau", Picture = "https://en.wikipedia.org/wiki/The_Room#/media/File:TheRoomMovie.jpg", Trailer = "https://www.youtube.com/watch?v=tfMTHIwTUXA" });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 2, Title = "Harry Potter and the Goblet of Fire", Genre = "Fantasy/Adventure", ReleaseYear = 2005, Director = "Mike Newell", Picture = "https://en.wikipedia.org/wiki/Harry_Potter_and_the_Goblet_of_Fire_(film)#/media/File:Harry_Potter_and_the_Goblet_of_Fire_Poster.jpg", Trailer = "https://www.youtube.com/watch?v=3EGojp4Hh6I", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 3, Title = "Harry Potter and the Philosopher's Stone", Genre = "Fantasy/Adventure", ReleaseYear = 2001, Director = "Chris Columbus", Picture = "https://en.wikipedia.org/wiki/Harry_Potter_and_the_Philosopher%27s_Stone_(film)#/media/File:Harry_Potter_and_the_Philosopher\'s_Stone_banner.jpg", Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo", FranchiseId = 2 });

            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 1, Name = "Harry Potter", Alias = "The boy who lived", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Harry_Potter_(character)#/media/File:Harry_Potter_character_poster.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 2, Name = "Johnny", Gender = "Male", Picture = "https://hero.fandom.com/wiki/Johnny_(The_Room)?file=Tommywiseau.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 3, Name = "Severus Snape", Alias = "The man who died", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Severus_Snape#/media/File:Ootp076.jpg" });


        }
    }
}
