﻿using AutoMapper;
using EFCore_movies.Models;
using EFCore_movies.Models.DTOs.Character;
using System.Linq;

namespace EFCore_movies.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.MovieId).ToArray()))
                .ReverseMap();
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
