﻿using AutoMapper;
using EFCore_movies.Models;
using EFCore_movies.Models.DTOs.Franchise;
using System.Linq;

namespace EFCore_movies.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m=>m.MovieId).ToArray()))
                .ReverseMap();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
