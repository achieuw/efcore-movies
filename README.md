# Movie Characters API

## Description
This project is part of an educational series and therefore is for educational purposes only.
The assignment is to create a web API that handles data for movies, characters and their franchises.
The API is created with Visual Studio .NET 5 and uses a MSSQL server to store its data.


## Usage
Fork this repo and run it to try the endpoints using swagger. 


## Contributors
The contributers of this project is Aldin Drobic, Alexander Grönberg & Rasmus Möller.
